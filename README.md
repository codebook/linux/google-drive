# Google Drive on Ubuntu


_tbd_


    sudo apt install -y gnome-control-center gnome-online-accounts


_tbd_




## References


* [Easily Mount Google Drive in Ubuntu 16.04 without Third-party Apps](https://www.maketecheasier.com/mount-google-drive-ubuntu-1604/)
* [4 Unofficial Google Drive Clients for Linux](https://www.maketecheasier.com/google-drive-clients-linux/)
* [How To Access Your Google Drive on Ubuntu 16.04](http://www.omgubuntu.co.uk/2016/08/use-google-drive-ubuntu-16-04-linux-desktops)
* [Mount Your Google Drive on Linux with google-drive-ocamlfuse](http://www.omgubuntu.co.uk/2017/04/mount-google-drive-ocamlfuse-linux)
* [How to Use Google Drive on Linux](https://www.howtogeek.com/196635/an-official-google-drive-for-linux-is-here-sort-of-maybe-this-is-all-well-ever-get/)
* [Google Drive client for the commandline](https://github.com/odeke-em/drive)



